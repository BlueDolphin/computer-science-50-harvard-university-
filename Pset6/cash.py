from cs50 import get_float

quarter = 0
dime = 0
nickel = 0
penny = 0

# Get user input

change = get_float (" How much change is owed?: ")

while (change >= .25):
    change = change - .25
    quarter = quarter + 1

while (change >= .10):
    change = change - .10
    dime = dime + 1

while (change >= .05):
    change = change - .05
    nickel = nickel + 1

while (change >= .01):
    change = change - .01
    penny = penny + 1


print (quarter)
print (dime)
print (nickel)
print (penny)