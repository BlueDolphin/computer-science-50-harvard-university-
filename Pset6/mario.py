from cs50 import get_int

# Get user input
height = get_int("Height of the pyramid?: ")


# While loop for Height times

s = 0
while (s < height + 1):

    x = 0
    while (x < height - s):

        print(" ", end="")
        x = x + 1

    r = s
    while (r > 0):

        print("#", end="")
        r = r - 1

    s = s + 1
    print("")
