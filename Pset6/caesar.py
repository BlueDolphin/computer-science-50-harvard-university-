from cs50 import *
import sys

### get user input
plaintext = get_string ("What is your messege?: ")


### Get user key as cmd line argument
print ("This is your key: ", int(sys.argv[1]))

### Define key
key = int(sys.argv[1])


print("Your Cipher Text")

for a in plaintext:

    if (a.islower()) :

        p = ((((ord(a) - 97) + key) % 26) + 97)

        d = (chr(p))

        print ((d), end="")

    if (a.isupper()) :

        b = ((((ord(a) - 65) + key) % 26) + 65)

        u = (chr(b))

        print ((u), end="")

print()


