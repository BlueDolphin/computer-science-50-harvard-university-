from nltk.tokenize import sent_tokenize


def lines(a, b):
    """Return lines in both a and b"""

    # TODO
    lines_a = set(a.split("\n"))
    lines_b = set(b.split("\n"))

    return lines_a & lines_b



def sentences(a, b):
    """Return sentences in both a and b"""

    # TODO

    list_a = set(sent_tokenize(a))
    list_b = set(sent_tokenize(b))

    return list_a & list_b

# define a loop, to iterate all strings compare string values
def substring_tokenize(str, n):
    substrings = []

    for i in range(len(str) - n + 1):
        substrings.append(str[i:i + n])

    return substrings


def substrings(a, b, n):
    """Return substrings of length n in both a and b"""

    substrings_a = set(substring_tokenize(a, n))
    substrings_b = set(substring_tokenize(b, n))

    return substrings_a & substrings_b